---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# 1 Introduction

Sont décrits ici les différents processus de gestion de la pérennisation des données de Nakala 
ainsi que leur gestion par l’équipe de la TGIR Huma-Num.
La norme conceptuelle ISO 14721:2012 «Open Archival Information System (OAIS)» est utilisée ici
pour son modèle fonctionnel ainsi que pour son vocabulaire (termes notés en majuscules).

# 2 Les acteurs
Le diagramme ci-dessous présente les différents acteurs échangeant de l’information avec l’OAIS (l’archive Nakala). 

![oais-acteurs](../media/oais-acteurs.png)

## 2.1 L’archive
L’archive Nakala conserve et donne accès à des ressources scientifiques issues de la communauté
académique française en sciences humaines et sociale. Une ressource, dans Nakala, peut contenir
un ou plusieurs fichiers. Un ensemble de ressources peut former une collection. Les fichiers
peuvent être de tout type (Par exemple une image, un enregistrement audio, un
document textuel, etc.)

## 2.2 Les producteurs
Les producteurs de ressources sont :
les auteurs ou contributeurs responsables scientifiques de la collecte ou la production des ressources.

## 2.3 Les utilisateurs
Les ressources de l’archive Nakala sont à destination de la communauté scientifique SHS afin
d’alimenter sa base de connaissances. Le service Nakala fait en sorte que les données soient
utilisables pour cette communauté visée et soient présentées comme des objets scientifiques
et culturels en utilisant des formats et vocabulaires largement répandus dans cette communauté
et au-delà. (par exemple : le Dublin-core qualifié pour les métadonnées ou des référentiels
tels que ORCID pour identifier les personnes ou encore l’ISO-639-3 pour identifier les langues).

## 2.4 Le management
La gestion de l’archive Nakala est prise en charge par la TGIR Huma-Num qui est une unité du Centre National de la Recherche Scientifique (CNRS) rattachée à l’Institut des sciences humaines et sociales du CNRS (INSHS) et labellisée comme une Très Grande Infrastructure de Recherche (TGIR) par le Ministère de l’enseignement supérieur, de la recherche et de l’innovation (MESRI). 

# 3 Les paquets d’information
La norme OAIS distingue 3 formes de paquets d’information. Le « SIP » (Submission Information
Package) paquet d’information échangé entre un producteur et l’archives, le « DIP » (Dissemination
Information Package) paquet échangée entre l’archive et les utilisateurs et l’ « AIP » (Archival
Information Package) qui représente la forme du paquet d’information interne à l’archive. 

## 3.1 Paquet d’informations fourni par le producteur (SIP)
Les informations fournies dans un paquet soumis à l’archive Nakala par le producteur se répartissent en deux ensemble:

- un ou plusieurs fichiers de données
- des métadonnées

La forme d’empaquetage de ces informations dépend du mode de dépôt dans l’archives : 
via un formulaire web ou via des API.
Suivant le niveau de service, les contraintes qui s’appliquent à ces deux ensembles diffèrent sensiblement.

Niveau de service 1: 

- Les fichiers de données peuvent être de tout type
- Les métadonnées obligatoires sont au nombre de 5 (`nakala:title`, `nakala:created`, `nakala:type`,
`nakala:license` et `nakala:creator`)
- Des métadonnées facultatives peuvent être rajoutées. Elles doivent se confomer au modèle du Dublin-Core qualifié 

Niveau de service 2: 

- Les données doivent être exprimées dans des formats listés dans un référentiel de formats (publié sur la page
[https://facile.cines.fr](https://facile.cines.fr)).
- Aux métadonnées obligatoires du niveau de service 1, s’ajoutent des métadonnées supplémentaires 
exigées par le format SEDA2 (norme NF Z 44-022 « Modélisation des Échanges de DONnées pour 
l’Archivage » et la norme ISO 20614 « Data exchange protocol for interoperability et 
preservation »). 

## 3.2 Paquet d’information archivé (AIP)
Le processus d'ingestion de Nakala génère de nouvelles métadonnées notamment par l’analyse du
contenu des données ou par l’enrichissement effectué par des experts « données »
(Cf. 4.6.2 La part des experts « données » dans le processus d’ingestion).

### 3.2.1 Métadonnées additionnelles
Les métadonnées ajoutées lors du processus d’ingestion sont les suivantes.

Pour le niveau de service 1:

- identifiant pérenne (DOI)
- empreintes des fichiers (SHA1)
- date de soumission
- tailles des fichiers
- noms des fichiers
- identifiant du producteur

Pour le niveau de service 2 : 

- les métadonnées additionnelles du niveau de service 1
- identifiant du format
- règles de gestion (type de données, sort final, durée de conservation, communicabilité)

### 3.2.2 Versions des données

Lorsqu'un fichier d’une donnée publiée est modifié, une nouvelle version de la donnée est générée et toutes les anciennes versions restent accessibles. 

Pour le niveau 2, toute modification des métadonnées entraine une nouvelle version de celles-ci et toutes les anciennes versions sont conservées.

## 3.3 Paquet d’information diffusé (DIP)
Les données et métadonnées de Nakala sont rendues accessibles aux utilisateurs via différentes interfaces
qui permettent la recherche, le filtrage et la récupération de tout ou partie des informations. 
Cf « 4.5 Accès aux informations (ACCESS) » pour une description de celles-ci. 
Toutes les données et métadonnées sont accessibles. Les fichiers de données peuvent-être éventuellement 
soumis à un contrôle d’accès.

# 4 Description du processus d’ingestion dans Nakala

## 4.1 Schéma d’ensemble
Cette section décrit comment l’organisation de l’ingestion de données dans l’OAIS Nakala dans le contexte d’un OAIS.

![oais](../media/oais.png)



### 4.1.1 Les profils

Plusieurs profils de l’équipe de la TGIR Huma-Num sont impliqués dans le mécanisme d’ingestion de données de Nakala.

- Les experts de « domaines » de SHS garantissent la qualité des données 
diffusée. Ils gèrent également les priorités dans les entrées.

- Les experts « données » (documentalistes, archivistes) avec de bonnes connaissance du domaine SHS,
 sont en charge du contrôle des données et métadonnées.

- Les ingénieurs IT sont en charge du système d’information et de la maintenance du service.

### 4.1.1 Les niveaux de service

On distingue 2 niveaux de service pour la préservation dans Nakala.

- Le niveau 1 assure une préservation du train de bit sans engagement sur la lisibilité à long terme des données. 

- Le niveau 2 assure une préservation de l'information contenue dans les données et s'engage sur leur lisibilité 
sur le long-terme. Une partie des tâches propres à ce niveau de service est assurée par le recours à de la
prestation de service auprès du CINES

## 4.2 Réception des données (ENTRY)

La phase de réception des données correspond à l’entité « ENTRY » dans le modèle OAIS.
Dans Nakala, le paquet d’information soumis SIP est composé de fichiers de données et
de métadonnées. Son ingestion peut suivre différents chemins:
Dépôt par l’auteur par le biais d’une interface web ou par le biais d’API

- Pour le niveau de service 1, des contrôles automatiques sont opérés qui conditionnent la poursuite du processus d’ingestion (vérification de la présence des 5 métadonnées obligatoires, vérifications syntaxiques sur l’expression des métadonnées, vérification de l’identité du producteur). En cas d’échec à un contrôle le SIP n’est pas créé et des messages d’erreurs sont envoyés au producteur.
- Pour le niveau de service 2, en plus des contrôles automatiques du niveau 1, un spécialiste « données » de la TGIR est désigné en charge du dépôt et la poursuite du processus d’ingestion sera conditionnée au résultat de son audit.

## 4.3 Stockage des informations (STORAGE)
La phase de stockage correspond à l’entité « ARCHIVAL STORAGE » du modèle OAIS. 
Le processus de stockage consiste en : 

- Copie des fichiers de données sur disque.
- Enregistrements des métadonnées (fournies par le producteur ainsi que celles fournies par le système) dans le SGBD.
- Écriture des opérations dans un journal horodaté
- Des opérations de réplication et de sauvegarde.

## 4.4 Gestion de données (MANAGEMENT)
Cette phase correspond à l’entité « DATA MANAGEMENT » du modèle OAIS.
L’ensemble des métadonnées soumises par le producteur, calculées lors des contrôles 
et éventuellement ajoutées par un expert « données » sont soumises à cette entité
pour une mise à jour de sa base de données (ElasticSearch). 
C’est cette base de données qui permet la recherche par des utilisateurs.

## 4.5 Accès aux informations (ACCESS)
Cette phase correspond à l’entité « ACCESS » du modèle OAIS.
Nakala permet plusieurs types d’accès à ses archives à travers des outils maintenus par Huma-Num. 

- Interface web
- SPARQL endPoint
- OAI-PMH
- APIs

## 4.6 Administration du système d’information (ADMINISTRATION)
Cette phase correspond à l’entité « ADMINISTRATION » du modèle OAIS.

### 4.6.1 Part des experts « domaine » dans le processus d’ingestion
Les experts « domaine » sont consultés lors des audits de qualité menés par les experts « données »
afin d'apporter leurs connaissance du domaine scientifique dans l'évaluation des formats, des modèles 
et des utilisés. Ils sont également sollicités aussi pour définir des priorités dans les entrées.

### 4.6.2 La part des experts « données » dans le processus d’ingestion
Les experts « données » (documentalistes, archivistes) avec de bonnes connaissance du domaine SHS,
sont en charge du contrôle des données et métadonnées. Ils intervienent dés lors qu'un producteur demande 
le niveau de service 2. Ils auditent les données et accompagnent les producteurs dans
l'amélioration de leur qualité. Ils peuvent solliciter pour leurs audits des experts « domaine ».

### 4.6.2 La part des experts « IT » dans le processus d’ingestion 
Les ingénieurs IT sont en charge du système d’information et de la maintenance du service.

## 4.7 Planification de la pérennisation (PRESERVATON PLANNING)
Cette phase correspond à l’entité « PRESERVATON PLANNING » du modèle OAIS.

# 5 Responsabilités de l’OAIS Nakala
Dans cette section, nous relions divers aspects des processus de gestion de Nakala à la liste des « responsabilités obligatoires » (énumérées dans la norme OAIS) pour une archive. 

 1. **Négocier avec les Producteurs d’information et accepter les informations appropriées de leur part**
La TGIR Huma-Num dialogue avec les communautés de recherche en SHS (notament par des [consortiums](https://www.huma-num.fr/les-consortiums-hn/) qu'elle finance) pour identifier les formats de représentation des informations qu’elle utilisent et mène, conjointement avec le CINES, d’éventuelles études pour évaluer ces formats vis à vis de leur capacité à être conservés sur le long terme ainsi que pour identifier les contrôles à effectuer pour leur acceptation dans l’archive.

 2. **Acquérir une maîtrise suffisante de l’information fournie, au niveau requis pour pouvoir en garantir la Pérennisation**.
Le processus d'ingestion de Nakala est supervisé par les experts « données » d’Huma-Num. 
Les métadonnées sont définies dans un premier temps par les auteurs. Elles sont complétées ensuite par le résultat
des contrôles puis éventuellement directement par les experts « données » d’Huma-Num.
Huma-Num est responsable de la conservation et de l’accès aux données publiées dans Nakala: 
ce qui lui donne le droit de modifier leur format (niveau de service 2) en fonction des nouveautés technologiques
ou de l'obsolescence. Les méta-données sont exprimées dans un modèle RDF et une syntaxe XML 
qui permettraient de recréer facilement la base de données si besoin.

 3. **Déterminer, soit par elle-même, soit en collaboration avec d’autres, quelles communautés doivent constituer la Communauté d’utilisateurs cible en mesure de comprendre l’information fournie, définissant ainsi sa Base de connaissance.**
La TGIR Huma-Num finance des consortiums disciplinaires afin de maintenir le lien avec ces communautés et 
de garantir l'adéquation entre les choix de représentation des connaissance et leur bonne compréhension et utilisation. 

 4. **Assurer que l’information à pérenniser est Immédiatement compréhensible pour la Communauté d’utilisateurs cible. En particulier, la Communauté d’utilisateurs cible devrait être en mesure de comprendre les informations sans recourir à des ressources particulières telles que l’assistance des experts ayant produit ces informations.**
Une attention particulière est portée, au niveau de service 2, à travers les audits des experts "données" et "domaine",
sur les choix des formats de représentation, la précision, l'exactitude et la complétude des informations.

 5. **Appliquer des politiques et des procédures documentées garantissant la pérennisation de l’information contre tout imprévu dans les limites du raisonnable, y compris la disparition de l’Archive, en garantissant qu’elle n’est jamais détruite sans autorisation en lien avec une politique validée. Il ne devrait pas y avoir de destruction adhoc.** 
Le niveau de service 2 conditionne la destruction des données à l'obtention d'une autorisation
par l'administration des archives. Dans tous les cas la destruction d'information est tracée, et en cas d'accès
aux données présente une "pierre tombale" qui affiche des informations minimales (identification, dates de suppression et 
forme de citation).

 6. **Rendre l’information pérennisée disponible pour la Communauté d’utilisateurs cible et faire en sorte que la diffusion de copies des Objets-données initialement versés soit tracée pour prouver l’Authenticité de l’information.**
Les informations de provenance et d'intégrité sont à tout moment disponibles dans les métadonnées publiées
et sont vérifiées régulièrement. 

# 6 Procédures avec recours à de la prestation de service
Cette section décrit les tâches confiées au prestataire de service
le CINES (Centre Informatique National de l’Enseignement Supérieur) et ne concerne que le niveau de service 2. Les détails
des opérations est décrit dans le cadre d'une convention signée entre le CNRS (Huma-Num) et le CINES.

- Les données sont recopiées sur le site du CINES (Montpellier) avec un exemplaire sur disque et 2 exemplaires
sur bandes en robotique.

- Les données doivent  être validées par l'outils de validation "Facile" (basé entre autres sur les briques logicielles
 DROID, JHOVE, ImageMagick...) sur la base de la liste des formats acceptables (Cf. page [https://facile.cines.fr](https://facile.cines.fr)) 
 
- En cas de détection d'obsolescence sur des formats utilisés qui pourait être identifiés par des experts de « domaines » ou
des experts IT (d'Huma-Num ou du CINES), le CINES peut mettre en oeuvre des opérations de transformation vers d'autres formats
à identifier aupréalable. 
