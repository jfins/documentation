---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Organisation de la documentation de NAKALA et des bonnes pratiques de description

--

## Enjeux

Du point de vue de l'**utilisateur**, il s'agit de :

- Être mieux accompagné dans le processus de dépôt et de gestion de ses données dans Nakala.
- Faire réfléchir les communautés de chercheurs aux choix et aux implications de la description de leurs données.

Du point de vue d'**Huma-Num**, cela aura pour heureuse conséquence de :

- Homogénéiser la description des données dans NAKALA.
- D'expliciter une politique claire vis-à-vis des champs et de leur utilisation dans NAKALA.
- Marquer l'engagement d'Huma-Num dans l'accompagnement des communautés dans la perspective de la certification CoreTrust Seal.



## Proposition de structuration de la documentation (4 parties)

### I -  Documentation pratique du service : prise en main.

Cette partie est déjà rédigée et apparaît dans la documentation en ligne à l'adresse suivante : [https://documentation.huma-num.fr/nakala/](https://documentation.huma-num.fr/nakala/). Elle concerne à proprement parler la description du service, sa prise en main et son utilisation. 
Elle doit s'enrichir au fur et à mesure des remarques / questions des utilisateurs.

### II - Préparation des données.

L'idée est de décrire les actions à envisager avant le dépôt dans NAKALA 

Une ébauche est disponible à l'adresse suivante 
[https://documentation.huma-num.fr/meta/nakala-preparer-ses-donnees/](https://documentation.huma-num.fr/meta/nakala-preparer-ses-donnees/)


### III - Bonnes pratiques de description des données. 

Un guide de catalogage qui pourrait être structuré en deux sous-parties distinctes s'adressant
à des types d'utilisateurs aux compétences différentes. 
Une ébauche de ce document est consultable à l'adresse suivante 
[https://documentation.huma-num.fr/meta/nakala-guide-de-description/](https://documentation.huma-num.fr/meta/nakala-guide-de-description/)

**1. Métadonnées obligatoires et guide de bonnes pratiques.**

Cette partie du guide de catalogage concernerait davantage des utilisateurs
peu au fait des implications de description des données et qui s'en tiendraient
aux métadonnées obligatoires de NAKALA.

Elle formulerait les grands principes d'une description et proposerait des définitions et justifications
des différents champs. 
Pour faciliter leur codage, les différentes étapes pourront être accompagnées d'exemples concrets de dépôts.

Sur ces métadonnées, un certain nombre de questions se posent vis-à-vis des champs obligatoires de NAKALA.
Cf les remarques et questions dans l'ébauche.

**2. Métadonnées spécialisées et avancées.**

Cette partie du guide de catalogage concernerait davantage des utilisateurs "experts" qui
souhaitent une description fine de leurs données lors du dépôt dans NAKALA. Ici se pose la
question de savoir si tous les champs proposés sont pertinentes dans le cadre de NAKALA.
Nous questionnent, en particulier : 

- Les **champs redondants** avec les métadonnées obligatoires de NAKALA. Y a-t-il une utilité à conserver ces champs redondants ? Ex : dcterms:creator ; dcterms:title ; dcterms:created ; etc. 
- Les **métadonnées de relations spécialisées** : est-ce que cela n'est pas déjà pris en compte par NAKALA, du fait de son organisation même ? Ex : le versionning (dcterms:isVersionOf). 
- Les **champs foaf**
- Quelques **types d'encodages** proposés (xsd:ID, xsd:IDREF,...)

### IV - Foire aux questions, évolutions de NAKALA et Schéma OAIS

#### Foire aux questions

L'idée est d'expliciter le "contrat" que l'on passe avec les utilisateurs de NAKALA 

Une ébauche est disponible à l'adresse suivante 
[https://documentation.huma-num.fr/meta/nakala-faq/](https://documentation.huma-num.fr/meta/nakala-faq/)


#### Schéma OAIS

Description des transferts de responsabilité suivant le modèle OAIS 
[https://documentation.huma-num.fr/meta/nakala-oais](https://documentation.huma-num.fr/meta/nakala-oais)


#### Politique d'évolution de NAKALA

Description de la politique d'évolution des contenus de NAKALA  
[https://documentation.huma-num.fr/meta/nakala-evolutions](https://documentation.huma-num.fr/meta/nakala-evolutions) 

