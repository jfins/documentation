---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Circuit de dépôt de données dans NAKALA

Un dépôt de données dans NAKALA s'effectue en plusieurs étapes :

* La demande d'ouverture de compte dans NAKALA  
* La préparation des données et leur dépôt dans l'entrepôt
* La curation automatisée et manuelle de premier niveau
* Le cas échéant une demande de préservation sur le long terme et son étude par le pôle accompagnement de données 
* Si la demande de préservation sur le long terme est acceptée, les données sont déposées au CINES


![Circuit de dépôt](../media/meta/chaine_preservation_hn_v7_fr.png)

## Demande d'ouverture de compte

La demande d'ouverture de service se fait via la [demande de création d'un compte](https://documentation.huma-num.fr/nakala/#utilisation-de-nakala). La demande est évaluée par le [comité de la grille](https://documentation.huma-num.fr/meta/cogrid/) d'Huma-Num.  
La demande d'ouverture de service n'est effectuée qu'une seule fois.

## La préparation des données et leur dépôt dans l'entrepôt

### Préparer les données
Avant le dépôt de données dans NAKALA, il est nécessaire d'entamer un processus de préparation des données. Pour cela, vous pouvez consulter le [guide de préparation](https://documentation.huma-num.fr/meta/nakala-preparer-ses-donnees/) qui vous indique les étapes à réaliser avant le dépôt.

### Décrire les données
De même, il est nécessaire de rassembler les informations nécessaires à la description des données (i.e. les métadonnées) et de les préparer pour qu'elles soient bien adaptées au modèle utilisé par NAKALA. Le [guide de description](https://documentation.huma-num.fr/meta/nakala-guide-de-description) vous donne toutes les indications nécessaires. 

### Déposer les données
Une fois ces étapes réalisées, vous pouvez alors déposer vos données dans NAKALA en vous aidant de la [documentation](https://documentation.huma-num.fr/nakala/) associée. 


## La curation automatisée et manuelle de premier niveau associée

Une curation est effectuée sur les dépôts de NAKALA :
- Lors du dépôt, l'entrepôt effectue un [ensemble de contrôles](https://documentation.huma-num.fr/meta/nakala-faq/#au-moment-du-depot) qui doivent être positifs pour pouvoir déposer la donnée.
- Huma-Num réalise également des [contrôles a posteriori](https://documentation.huma-num.fr/meta/nakala-faq/#controles-reguliers). Ces analyses manuelles permettent de faire un retour au déposant sur la qualité de ses dépôts. 
Huma-Num étudie la possibilité de mettre en place un indice de qualité calculé sur un ensemble de critères contrôlables.

## Le cas échéant une demande de préservation sur le long terme et son étude par le pôle accompagnement de données 

Lorsque le producteur effectue une demande de préservation sur le long terme d'une collection, le pôle "accompagnement des utilisateurs" effectue un audit approfondi des données à préserver sur le long terme (Cf. la [convention](https://documentation.huma-num.fr/meta/partenariat-hn-cines/) passée entre Huma-Num et le CINES). 

En premier lieu, il s'agit de vérifier la validité des formats de fichiers et les mettre en conformité avec les pré-requis exigés par le CINES   

Par ailleurs, une attention particulière sera portée aux métadonnées descriptives qui doivent être adaptées aux besoins particuliers de la préservation sur le long terme. En effet, l'organisation intellectuelle des données peut être amenée à évoluer ainsi que le contenu de leur description. 

Cette démarche nécessite une série d'échanges entre le producteur et le pôle Données de la TGIR Huma-Num.
A la fin de ce processus, une décision est prise par le "[comité de liaison](https://documentation.huma-num.fr/meta/partenariat-hn-cines/#comite-de-liaison)" sur l'opportunité de lancer une préservation à long terme des données. 

## Si la demande de préservation sur le long terme est acceptée, les données sont déposées au CINES

Les données sont déposées au CINES par l'équipe de Huma-Num et le certificat d'archivage fourni par le CINES est intégré aux métadonnées des données déposées. 

Le suivi et les mises à jours sont effectués par l'équipe de Huma-Num en collaboration avec le producteur des données. 

A noter que du point de vue du CINES, Huma-Num est considéré comme l'interlocuteur concernant ces données : par conséquent, toutes demandes concernant ces données passeront par Huma-Num (e.g. demandes d'accès à la copie déposée, supression, restitution, transfert, etc. )







