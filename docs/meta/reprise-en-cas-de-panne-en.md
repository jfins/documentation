---
lang: en
---

!!! Note  
    Document in progress


# Disaster Recovery Plan

## Hardware failure(s) 

Huma-Num regularly updates its infrastructure. 
Thus, all Huma-Num's equipment is covered by the guarantee provided by the manufacturers: in case of failure, an intervention will be carried out in less than 24 hours (D+1) for working days.  

Similarly, the equipment managed by the [CCIN2P3 computing center](https://cc.in2p3.fr/) which hosts the Huma-Num infrastructure is covered by equivalent guarantees.   

It can be noted that overall, with a hindsight of about ten years, hardware failures are very rare today. 
However, a failure is always possible: even if Huma-Num cannot formally guarantee a totally permanent operation, all precautions are taken to minimize service interruptions.  

### In case of [firewall](https://en.wikipedia.org/wiki/Firewall_(computing)) failure

Huma-Num is equipped with a firewall that allows it to refine the filtering performed for its specific needs. It is used in addition to the general firewall of the computing center that hosts Huma-Num's infrastructure.

The firewall used by Huma-Num is not redundant for the moment, it is planned to consider it within the framework of the COMMONS project (financed by the PIA3) which will start at the end of 2021. 
In case of failure, while waiting for the intervention of the manufacturer, it is possible to continue to operate in a slightly degraded way in terms of filtering by using only the protection of the firewall of the computing center. 


### In the event of a network component failure 

The network components are managed by the computing center that hosts the Huma-Num infrastructure. 
In addition to the above-mentioned guarantee, the computing center has these components in stock, which are standardized for the computing center, and its maintenance personnel intervene as soon as the failure is detected.   


### In case of a physical machine failure 

Most of Huma-Num's services are redundant on different physical machines (e.g. NAKALA, ISIDORE etc.). In case of failure of a machine, the other instance of the service takes over. 

Moreover, the services offered by Huma-Num are virtualized (i.e. hosted on virtual machines): regular backups (snapshots) of these machines are made which allows them to be restored on another machine in case of failure. This restoration process is not immediate and may cause the service to be unavailable for some time. It is the same for the virtual machines proposed by Huma-Num which are managed by the users. 


### In case of a [NAS](https://en.wikipedia.org/wiki/Network-attached_storage) failure

The NAS used by Huma-Num integrate high level security devices, in particular :  
- the disk "controller" part is doubled;    
- the network part is also redundant;  
- the NAS uses [RAID](https://en.wikipedia.org/wiki/RAID) technology which allows it to function in case of failure of one or more storage devices.  

As part of the service contract associated with the NAS, the manufacturer is notified in real time of any problems (hardware or software) and its personnel are authorized to intervene in the data center.   

In the event of a major disaster that is not covered by these security features, it will still be possible to restore the data from the backups, but this process will take some time before the data is made available again. 


## Use of backups 


### In case of data problems (deletion, replacement etc.)

Several levels of security are proposed by Huma-Num for the data stored on its infrastructure: 
- for data stored on NAS type devices (e.g. sharedoc or shared web hosting), an image (snapshot) is automatically made by the NAS at regular intervals (every hour) which allows to restore very quickly a modified or deleted file. Of course, the capacity of the disks not being infinite, these numerous images cannot be kept indefinitely and are replaced as and when needed. If data has been erased or modified for a period of time that exceeds this image creation cycle, it is possible to restore it from the backups made on magnetic tape, but this requires an intervention that will take more time. 
- For data stored on Huma-Num Box devices, backups (e.g. number of copies, versions etc.) are ensured by the policies set up by the Active-Circle system software for each share. File restoration is also ensured by the Active Circle software. 
- Data stored on the "local" disks of the machines is generally not backed up. However, it is possible to set up backups with the support of Huma-Num. 


### In case of a problem with a database 

Databases managed by Huma-Num (e.g. for shared web hosting) are backed up daily: a database can therefore be restored with the content saved the day before.  

For databases hosted in virtual machines, the managers of these machines must set up such backups with the support of Huma-Num if needed. 





