---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Le comité de la grille de Huma-Num 

Les missions du comité de la grille de Huma-Num sont les suivantes : 
-  examiner et instruire toutes les demandes d'ouverture de service en conformité avec la [politique générale](https://www.huma-num.fr/presentation/) d'utilisation des services de Huma-Num. Le suivi des demandes est effectué avec un outil de [gestion de tickets](https://support.huma-num.fr/otrs/index.pl) qui permet de structurer les échanges avec les demandeurs ; 
-  traiter lorsque cela est nécessaire (e.g. dépassement de capacité) l'évolution de l'offre pour l'accès à des services déjà ouverts ;
-  analyser tous les sujets relatifs à la maintenance et à l'évolution des services ;
-  saisir le conseil scientifique en cas de besoin d'un avis scientifique sur la validité d'une demande. 

Le comité est composé de membres représentant les différents pôles de Huma-Num et d'au moins un représentant de la direction. 

Le comité se réunit toutes les semaines. 


