---
title: Voyant Tools
lang: fr
description: Voyant Tools, un service tiers hébergé par Huma-Num et lien vers sa documentation.
---

# Voyant Tools

!!! attention "À savoir"
    Les services tiers sont des outils et des plateformes dont la conception,
    la maintenance et la documentation sont assurés par  d'autres instances ou
    organisations que la TGIR Huma-Num. La TGIR Huma-Num met à disposition des
    machines virtuelles hébergées sur son infrastructure pour assurer l'accessibilité
    de ces outils et soutenir leur usage par la communauté SHS.

Voyant Tools est un environnement en ligne de lecture et d’analyse de textes numériques développé par Stéfan Sinclair (†), McGill University et Geoffrey Rockwell, University of Alberta (Canada).

Huma-Num héberge l'un des 3 miroirs internationaux de cet outil. La traduction en français de l'interface a été réalisée par Aurélien Berra. Le code source est disponible sur [GitHub](https://github.com/sgsinclair/Voyant).

!!! note
    - Accéder à l'instance de Voyant Tools hébergée par la TGIR Huma-Num : [https://voyant-tools.huma-num.fr](https://voyant-tools.huma-num.fr)
    - Voir [la documentation de Voyant Tools](https://voyant-tools.huma-num.fr/docs/#!/guide)
