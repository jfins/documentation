# GitLab : hébergement, versionning et partage de code

L’instance GitLab d’Huma-Num permet l’hébergement sécurisé et le partage maîtrisé de fichiers de code selon le protocole _git_.

Il s’agit d’une implémentation du logiciel [Gitlab](https://about.gitlab.com/).

Les principales fonctionnalités sont la gestion de version et des dépôts (git), l’intégration continue, la génération de sites web (pages), la gestion de tickets (issues).

**Demander l’ouverture d’un compte Gitlab** : la demande d’un compte Gitlab se fait à partir de l’interface [HumanID](https://humanid.huma-num.fr). Pour cela, le cas échéant il est nécessaire de disposer d’un compte HumanID ([voir la documentation](humanid.md)).

Accès au service hébergé par Huma-Num : [gitlab.huma-num.fr](https://gitlab.huma-num.fr/).
